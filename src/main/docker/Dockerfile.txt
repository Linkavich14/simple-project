FROM dockerfile/java:oracle-java8

VOLUME /tmp
ADD simple_project.jar /opt/simple_project/
EXPOSE 8080
WORKDIR /opt/simple_project/
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-Xms512m", "-Xmx1g", "-jar", "simple_project.jar"]
